<?php

/**
 * @file
 * Hook implementations of the commerce_order_autovalidate module.
 */

/**
 * Implements hook_cron().
 *
 * Auto-validate all orders with completed payments.
 */
function commerce_order_autovalidate_cron() {
  $query = \Drupal::database()->select('commerce_order', 'co');
  $query->leftJoin('commerce_payment', 'cp', 'cp.order_id = co.order_id');
  $query->condition('co.state', 'validation');
  $query->condition($query->orConditionGroup()
    ->condition('co.total_price__number', 0)
    ->condition('cp.state', 'completed')
  );
  $query->fields('co');
  $result = $query->execute()->fetchAllAssoc('order_id');
  $order_ids = array_keys($result);
  if (empty($order_ids)) {
    return;
  }

  $order_storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
  /** @var \Drupal\commerce_order\Entity\OrderInterface[] $orders */
  $orders = $order_storage->loadMultiple($order_ids);
  foreach ($orders as $order) {
    $transition = $order->getState()->getWorkflow()->getTransition('validate');
    if ($transition && $order->isPaid()) {
      $order->getState()->applyTransition($transition);
      $order->save();
      \Drupal::logger('commerce_order_autovalidate')->info('Automatically validated order @order', ['@order' => $order->id()]);
    }
  }
}
